import {surface, circumference} from "./js/circle";
import "./css/style.css"
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"

addEventListeners();


function calculate(event:Event) {
  let radius = (event.target as HTMLInputElement).valueAsNumber;
  document.getElementById("surface")!.textContent = surface(radius).toFixed(2);
  document.getElementById("circumference")!.textContent = circumference(radius).toFixed(2);
}

function addEventListeners() {
  document.getElementById("radius")?.addEventListener("blur", calculate);
}

