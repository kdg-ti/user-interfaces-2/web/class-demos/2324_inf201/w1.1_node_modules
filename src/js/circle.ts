export function surface(radius:number) {
  return radius * radius * Math.PI;
}

export function circumference(radius:number) {
  return radius * 2 * Math.PI;
}

