// using javacript from another file in node.js using modules
//import {surface,circumference} from "./circle.ts";
import * as circle from "./circle.js";

console.log("Surface: " + circle.surface(5)+ ".") ;
console.log("Circumference: " + circle.circumference(5));