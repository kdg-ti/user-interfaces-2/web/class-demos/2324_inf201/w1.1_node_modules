import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";

const config = {
  devtool: "source-map",
  mode: "development",
  plugins: [
    new HtmlWebpackPlugin({template :"./src/html/index.html" }),
    new MiniCssExtractPlugin()
  ],
  resolve:{
    extensions:[".js",".ts"],
    extensionAlias:{".js":[".js",".ts"]}
  },
  module:{
    rules:[
      {
        test: /\.ts$/i,
        use:["ts-loader"],
        exclude:/node_modules/
      },
      {
        test: /\.css$/i,
        use:[MiniCssExtractPlugin.loader,"css-loader"]
      },
      {
        test: /\.html$/i,
        use:["html-loader"]
      },
      {
        test: /\.(png|jpe?g|gif|ttf)$/i,
        type:"asset"
      },
    ]
  },
  devServer: {
    static: {
      directory: path.resolve("dist")
    },
    open: true,
    hot: false
  },
  output: {
    clean:true
  }
}
export default config;